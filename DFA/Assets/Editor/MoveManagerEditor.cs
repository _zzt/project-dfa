﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(MoveManager))]
public class MoveManagerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        MoveManager owner = (MoveManager)target;

        Vector3 euler = EditorGUILayout.Vector3Field("EulerAngle", owner.Dest.eulerAngles);
        owner.Dest = Quaternion.Euler(euler);

        if (GUILayout.Button("SendMessage(Jump)"))
        {
            owner.TurnOn();
        }
        if (GUILayout.Button("SendMessage(Dive)"))
        {
            owner.TurnOn(true, false);
        }
        if (GUILayout.Button("SendMessage(Jump-Dive)"))
        {
            owner.TurnOn(true, true);
        }
    }
}