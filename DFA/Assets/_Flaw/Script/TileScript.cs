﻿using UnityEngine;
using System.Collections;

public class TileScript : MonoBehaviour {
    
    
    // Use this for initialization
    /*
    void Awake () {
        foreach (Rigidbody rigidBody in transform.GetComponentsInChildren<Rigidbody>())
        {
            rigidBody.detectCollisions = false;
        }
    }
    
    // Update is called once per frame
    void Update () {
        
    }
    */

    public enum TileType
    {
        Ordinary,
        Spring,
        Nonexistent
    }

    bool destroying = false;     // 넌 이미 죽어있나?
    bool destroyed = false;     // 진짜 쥬금?

    bool used = false;          // 씀?


    float deltaTime = -1f;

    Vector3 t_forcePosition;
    float t_forceIntensity;

    int ran_shake;

    public TileType Type;

    void Start()
    {
        if (this.Type == TileType.Nonexistent)
            destroyed = true;

        ran_shake = Random.Range(0, 360);
    }


    public void Reset(TileType tileType)
    {
        this.Type = tileType;
        this.destroyed = false;
        this.destroying = false;
        this.used = false;          // 씀?
        foreach (Rigidbody rigidBody in transform.GetComponentsInChildren<Rigidbody>())
        {
            if (rigidBody.name == "Quad")
                continue;
            else if (rigidBody.name == "Cube Lower")
                rigidBody.transform.localPosition = new Vector3(0, -45, 0);
            else if (rigidBody.name == "Cube Upper")
                rigidBody.transform.localPosition = new Vector3(0, 0, 0);

            rigidBody.isKinematic = true;
            rigidBody.rotation = Quaternion.Euler(0,0,0);
            
        }

        deltaTime = -1f;

        Start();
    }

    public void DestroyTile(Vector3 forcePosition, float forceIntensity = 500000)
    {
        if (destroying) return;
        destroying = true;

		this.transform.FindChild ("Quad").GetComponent<MeshCollider> ().isTrigger = true;
      /*  foreach (Rigidbody rigidBody in transform.GetComponentsInChildren<Rigidbody>())
        {
            //rigidBody.detectCollisions = true;
            rigidBody.isKinematic = false;
            rigidBody.AddForce(forceIntensity * Vector3.Normalize(transform.position - forcePosition) / Mathf.Pow(Vector3.Magnitude(transform.position - forcePosition), 2), ForceMode.Impulse);
            rigidBody.AddTorque(Random.Range(-1000f, 1000f), Random.Range(-1000f, 1000f), Random.Range(-1000f, 1000f), ForceMode.Impulse);
        }*/

        t_forceIntensity = forceIntensity;
        t_forcePosition = forcePosition;

        DyingTile();
    }

    public void DyingTile()
    {
        deltaTime = Time.realtimeSinceStartup;
    }

    void Update()
    {
        if (deltaTime < 0)
        {
            return;
        }
        else
        {

            ShakingTile(Time.realtimeSinceStartup - deltaTime);
        }
        
    }

    private void ShakingTile(float DeltaTime)
    {
        foreach (Rigidbody rigidBody in transform.GetComponentsInChildren<Rigidbody>())
        {
            rigidBody.position = rigidBody.position + new Vector3(Mathf.Sin(DeltaTime * 25 + (float)ran_shake) * 0.3f, 0, 0);

            if(DeltaTime > 4.0f){
                rigidBody.isKinematic = false;

                rigidBody.AddForce(t_forceIntensity * Vector3.Normalize(transform.position - t_forcePosition)
                    / Mathf.Pow(Vector3.Magnitude(transform.position - t_forcePosition), 2), ForceMode.Impulse);
                rigidBody.AddTorque(Random.Range(-1000f, 1000f), Random.Range(-1000f, 1000f), Random.Range(-1000f, 1000f), ForceMode.Impulse);

                deltaTime = -1f;
                destroyed = true;
            }
        }        
    }

    public bool isdestroyed()
    {
        return destroyed;
    }

    public bool isused()
    {
        bool use = used;
        used = true;

        return use;
    }

    public TileType getTileType()
    {
        return Type;
    }

}
