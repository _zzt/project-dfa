﻿using UnityEngine;
using System.Collections;



public class FighterAnimationDemo : MonoBehaviour
{

    public Animator animator;
    private MoveManager MoveManager;

    private Transform defaultCamTransform;
    private Vector3 resetPos;
    private Quaternion resetRot;
    private GameObject cam;
    private GameObject fighter;
    private Vector3 lerpto;
    private float yJumpTimer = 0f;  // 공중에 떠있는 동안 값이 계속 오르고 그거에 따라서 일을 한다. - 정호, Won
    private float diveStartY;           // 다이브 시작 지점


    private float jump_height = 120f;



    public enum JumpState
    {
        Not_Jumping,
        Leaping,
        Diving,
        DIE
    }

    JumpState jumpState = JumpState.Not_Jumping;

    void Start()
    {
        lerpto = new Vector3(0.0f, 0.0f);
        cam = GameObject.FindWithTag("MainCamera");
        defaultCamTransform = cam.transform;
        resetPos = defaultCamTransform.position;
        resetRot = defaultCamTransform.rotation;
        fighter = GameObject.FindWithTag("Player");

        MoveManager =  gameObject.GetComponent<MoveManager>();

        MoveManager.dest = new Vector3(0, 0, 0);
        MoveManager.Start();
        animator.SetTrigger("Spring");

        

        //fighter.transform.position = new Vector3(0,0,0);
    }

    IEnumerator Dive_reset()
    {

        yield return new WaitForSeconds(0.1f);

        animator.SetBool("Dive", false);
    }

    IEnumerator SetReturn()
    {

        yield return new WaitForSeconds(0.1f);

        animator.SetBool("Idle_return", false);
    }

    IEnumerator wait_dive()
    {

        GameObject a = GameObject.FindGameObjectWithTag("GameManager");

        yield return new WaitForSeconds(1f);

        a.SendMessage("Drop");

    }


    void Update()
    {

    }

    public void Jump_just(RaycastHit Hit, bool isclose = false)
    {
        Debug.Log("잠푸" + (isclose ? transform.position : Hit.transform.position));
        animator.SetTrigger("Jump_just");

        MoveManager.dest = this.transform.position;
        MoveManager.dest.y = jump_height;
        MoveManager.TurnOn();

        jumpState = JumpState.Leaping;

    }

    public void Jump_Attack(RaycastHit Hit, bool isclose = false)
    {
        Debug.Log("뛰어찍기" + (isclose ? transform.position : Hit.transform.position));
        animator.SetTrigger("Jump_Attack");

        MoveManager.dest = Hit.transform.position;
        MoveManager.dest.y = jump_height;
        MoveManager.TurnOn(true, true);

        jumpState = JumpState.Leaping;

    }

    public void Dive(RaycastHit Hit, bool isclose = false)
    {
        Debug.Log(Hit.transform.gameObject.name);
        Debug.Log("다이브!" + (isclose ? transform.position : Hit.transform.position));
        animator.SetBool("Dive", true);

        StartCoroutine(Dive_reset());

        MoveManager.dest = Hit.transform.position;
        MoveManager.dest.y = 0f;
        MoveManager.TurnOn(true, false);

        yJumpTimer = 0;
        diveStartY = transform.position.y;
        jumpState = JumpState.Diving;

    }

    public void Die()
    {
        animator.SetTrigger("DIE");
        jumpState = JumpState.DIE;

        Rigidbody player_body = this.GetComponent<Rigidbody>();
        player_body.useGravity = true;
        player_body.isKinematic = false;
        player_body.velocity = new Vector3(0f, 30f, 0f);
        //player_body.velocity = new Vector3();
    }

    public void Spring()
    {
        Debug.Log("Spring!");

        animator.SetTrigger("Spring");
        jumpState = JumpState.Leaping;

        MoveManager.dest = this.transform.position;
        MoveManager.dest.y = 200f;
        MoveManager.TurnOn(false, true, true);
    }

    public void SetGround()
    {
        animator.SetBool("Idle_return", true);
        StartCoroutine(SetReturn());
    }


    void OnGUI()
    {
        if (GUI.RepeatButton(new Rect(815, 535, 100, 30), "Reset Scene"))
        {
            defaultCamTransform.position = resetPos;
            defaultCamTransform.rotation = resetRot;
            fighter.transform.position = new Vector3(0, 0, 0);
            animator.Play("Idle");
        }

    }
}