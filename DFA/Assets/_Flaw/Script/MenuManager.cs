﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour {
	
	public GameObject SettingsMenu; // The settings Menu (desactivated by default)
	public GameObject HighScore; // The highscore (displayed in the settings menu)

	private bool GameLaunched = false; // bool to know if the player has clicked on play

	Text hscoretext; // Highscore.

	private Quaternion calibrationQuaternion; // The quaternion used to calibrate our vector direction.

	void AccCalibration()
	{
		// Here we will take the acceleration capted in the start of the game and make it the default
		// acceleration for the rest of the game, unless the player want to calibrate it again.

		Vector3 accelerationSnapshot = Input.acceleration; // Get acceleration input

		Quaternion rotateQuaternion = Quaternion.FromToRotation (new Vector3 (0.0f, 0.0f, -1.0f), accelerationSnapshot);
		calibrationQuaternion = Quaternion.Inverse (rotateQuaternion);
		// We then create a quaternion capable of calibration our future vector.

		//We save the Quaternion in PlayerPref. Considering you can only store floats and int in PlayerPref. We have
		// to store each part of the quaternion manually.
		PlayerPrefs.SetFloat("quadx", calibrationQuaternion.x);
		PlayerPrefs.SetFloat("quady", calibrationQuaternion.y);
		PlayerPrefs.SetFloat("quadz", calibrationQuaternion.z);
		PlayerPrefs.SetFloat("quadw", calibrationQuaternion.w);
	}

	void Start()
	{
		Time.timeScale = 1.0f; //Set Game speed to normal.

		// Then get the Highscore text component and update it.
		hscoretext = HighScore.GetComponent<Text>();
		hscoretext.text = "High score : " + PlayerPrefs.GetInt("HighScore");

		// Here we calibrate orientation for the first time.
		AccCalibration();
	}

	// These 4 function are UI related

	public void LaunchGame()
	{
		GameLaunched = true;
	}

	public void SetAA(float aa2)
	{
		// aa2 will be between 0 and 3 as set in the inspector.
		// QualitySettings.antiAliasing can only take 0, 2, 4 and 8 for value.

		int aa = (int)aa2; //we cast aa2 in integer
		aa *= 2; // multiply it by 2
		aa = (aa == 6 ? 8 : aa); // if aa is equal to 6, make it equal to 8.

		// That's how you go from value between 0 and 3 to value [0, 2, 4, 8].

		QualitySettings.antiAliasing = aa;
	}

	public void ShowParam()
	{
		//active the menu if it is desactivated and vice versa
		SettingsMenu.SetActive(!SettingsMenu.activeSelf);
	}

	public void SetAcc()
	{
		//Recalibrate the acceleration
		AccCalibration();
	}

	void Update()
	{
		float fog; 
		// Considering our fog is linear, the fog is "full" at a certain distance. (which is fogEndDistance) 

		if (GameLaunched)
		{
			// If the player has cliked on the play button, the fog will get thicker and thicker
			// When the fog will be the "thickest", the game will start.
			fog = RenderSettings.fogEndDistance;
			if (fog <= 0.0f)
				Application.LoadLevel("Flaw");
			else
				fog -= 150.0f * Time.deltaTime;
			// The "fogenddistance" is equal to 150 at the start of the game.
			//The line above is made to make it equal to 0 in 1 second.
			RenderSettings.fogEndDistance = fog; // aplly the fog.
		}
	}
}
