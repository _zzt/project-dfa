﻿using UnityEngine;
using System.Collections;

namespace HAJE.DFA
{
    public class PlayerDummyScript : MonoBehaviour
    {
        private bool isAttacking;
        private void Start()
        {
            this.isAttacking = true;
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (isAttacking == false)
                return;

            if (collision.gameObject.name == "Quad")
            {
                GameObject[] mobs = GameObject.FindGameObjectsWithTag("Mob");
                foreach (GameObject mob in mobs)
                {
                    Rigidbody rb = this.GetComponent<Rigidbody>();
                    Rigidbody mrb = mob.GetComponent<Rigidbody>();

                    if ((rb.position - mrb.position).magnitude < 10f)
                        mob.SendMessage("Kill", MobScript.DeathReason.Shockwave);
                }

                isAttacking = false;
            }
            else if (collision.gameObject.tag == "Mob")
            {
                Destroy(collision.gameObject);
            }
        }
    }
}