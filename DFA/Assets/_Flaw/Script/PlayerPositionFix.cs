﻿using UnityEngine;
using System.Collections;

public class PlayerPositionFix : MonoBehaviour {

	// Use this for initialization
	void Start () {         //캐릭터의 자체 Position 이 흔들려서 고정시킴. (다른데서는 이러지 마세요 망함.)
        transform.localPosition = new Vector3(0f, 0f, 0f);
	}
	
	// Update is called once per frame
	void Update () {
        transform.localPosition = new Vector3(0f, 0f, 0f);
	}
}
