﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


namespace HAJE.DFA
{
    public class TileManager : MonoBehaviour
    {
        public GameObject Player;
        public GameObject[] Tiles;
        public float[] Probability;

        private const int block_No = 100;
        private const int rowWidth = 10;
        private const float blockSize = 40f;

        //private List<GameObject> TileBlocks = new List<GameObject>();
        //private Queue<GameObject> garbage = new Queue<GameObject>();

        private Dictionary<TileScript.TileType, List<GameObject>> TileBlocks;
        private Dictionary<TileScript.TileType, Queue<GameObject>> garbage;

        bool awakened = false;

        void Awake()
        {
            Time.timeScale = 1.0f; // make the speed of the game normal.
            //RenderSettings.fogEndDistance = 10000.0f; // Make the fog really thick (to make a cool transition).

            TileBlocks = new Dictionary<TileScript.TileType, List<GameObject>>();
            garbage = new Dictionary<TileScript.TileType, Queue<GameObject>>();

            TileBlocks.Add(TileScript.TileType.Nonexistent, new List<GameObject>());
            TileBlocks.Add(TileScript.TileType.Ordinary, new List<GameObject>());
            TileBlocks.Add(TileScript.TileType.Spring, new List<GameObject>());
            garbage.Add(TileScript.TileType.Nonexistent, new Queue<GameObject>());
            garbage.Add(TileScript.TileType.Ordinary, new Queue<GameObject>());
            garbage.Add(TileScript.TileType.Spring, new Queue<GameObject>());

            InitSpawnTiles();
        }

        void InitSpawnTiles()
        {

            Vector3 pos = new Vector3(-125.0f, -20f, 20.0f); // Initial position based on start position of the player
            TileScript.TileType tiletype = TileScript.TileType.Nonexistent;

            for (int i = 0; i < block_No; ++i)
            {
                Vector3 newpos = pos + new Vector3(blockSize * (i % rowWidth), 0, blockSize * (i / rowWidth));

                tiletype = random_TileType();

                Object prefab = null;

                if (tiletype == TileScript.TileType.Nonexistent)
                    prefab = Tiles[0];
                else if (tiletype == TileScript.TileType.Ordinary)
                    prefab = Tiles[1];
                else if (tiletype == TileScript.TileType.Spring)
                    prefab = Tiles[2];


                //    TileBlocks.Add(Instantiate(prefab, newpos, Quaternion.identity) as GameObject);
                this.SpawnTile(tiletype, newpos);
                this.GetComponent<MobManager>().SpawnMob(MobScript.MobType.Normal, newpos + 25 * Vector3.up);
            }

            GameObject tile;
            /*
            if (garbage.Count != 0)
            {
                tile = garbage.Dequeue();
                tile.SetActive(true);
                tile.GetComponent<TileScript>().Reset(tileType);
            }
            */
            // As this function will only be called once in the beginning of the game, we can predict where we want our
            // buildings to be placed.
            awakened = true;
        }

        public void SpawnTile(TileScript.TileType tileType, Vector3 position)
        {
            Object TileObject;
            GameObject tile;

            if (garbage[tileType].Count != 0)
            {
                tile = garbage[tileType].Dequeue();
                tile.SetActive(true);
                tile.GetComponent<TileScript>().Reset(tileType);
            }
            else
            {
                if (tileType == TileScript.TileType.Nonexistent)
                    TileObject = Tiles[0];
                else if (tileType == TileScript.TileType.Ordinary)
                    TileObject = Tiles[1];
                else
                    TileObject = Tiles[2];

                tile = Instantiate(TileObject, position, Quaternion.identity) as GameObject;
                tile.GetComponent<TileScript>().Reset(tileType);
            }

            TileBlocks[tileType].Add(tile);

            tile.transform.position = position;

        }
        /*
        void ManageTiles()
        {
            for (int i = rowWidth - 1; i >= 0; --i)
            {
                Vector3 tilePos = TileBlocks[i].transform.position + new Vector3(0, 0, blockSize * (block_No / rowWidth));
                

                Object prefab = null;


                TileBlocks.Add(Instantiate(prefab, tilePos, Quaternion.identity) as GameObject);
                this.GetComponent<MobManager>().SpawnMob(MobScript.MobType.Normal, tilePos + 25 * Vector3.up);
                DestroyObject(TileBlocks[i]);
                TileBlocks.RemoveAt(i);
            }
        }*/


        void Start()
        {
            Debug.Assert(block_No % rowWidth == 0);
            Time.timeScale = 1.00f;

            Player = GameObject.FindGameObjectWithTag("Player");
        }

        void Drop()
        {

            /*
            for (int i = 0; i < 4 * rowWidth; i++)      // 캐릭터 뒤쪽 타일을 으앙 쥬김.
            {
                TileBlocks[i].GetComponent<TileScript>().DestroyTile(Player.transform.position - new Vector3(0f, 100f));

            }
            */

        }
        void Update()
        {

            /*
            if (awakened && Input.GetKeyUp(KeyCode.G))
            {
                for(TileBlocks)
                for (int i = 0; i < 6 * rowWidth; i++)
                    TileBlocks.GetComponent<TileScript>().DestroyTile(Player.transform.position - new Vector3(0f, 100f));
            }*/

            /*
            if (awakened && Player.transform.position.z > TileBlocks[0].transform.position.z + 100f)
            {
                Debug.Log(" dd");
                ManageTiles();
            }
            */

            Vector3 temp_position = new Vector3();
            bool Vanishment = false;

            List<TileScript.TileType> tiletypelist = new List<TileScript.TileType>(TileBlocks.Keys);

            for (int arr = 0; arr < tiletypelist.Count; arr++)
            {
                for (int i = 0; i < TileBlocks[tiletypelist[arr]].Count; i++)
                {
                    Vanishment = false;
                    /*if (distance_xz(TileBlocks[i].transform.position, Player.transform.position) > 200f)
                    {
                        TileBlocks[i].SetActive(false);
                        garbage.Enqueue(TileBlocks[i]);
                        TileBlocks.RemoveAt(i);
                        i--;
                    }*/

                    if (TileBlocks[tiletypelist[arr]][i].transform.position.x - Player.transform.position.x > 200f)
                    {
                        Vanishment = true;
                        temp_position = TileBlocks[tiletypelist[arr]][i].transform.position;
                        temp_position.x -= 400f;
                    }
                    else if (TileBlocks[tiletypelist[arr]][i].transform.position.x - Player.transform.position.x < -200f)
                    {
                        Vanishment = true;
                        temp_position = TileBlocks[tiletypelist[arr]][i].transform.position;
                        temp_position.x += 400f;
                    }
                    else if (TileBlocks[tiletypelist[arr]][i].transform.position.z - Player.transform.position.z > 200f)
                    {
                        Vanishment = true;
                        temp_position = TileBlocks[tiletypelist[arr]][i].transform.position;
                        temp_position.z -= 400f;
                    }
                    else if (TileBlocks[tiletypelist[arr]][i].transform.position.z - Player.transform.position.z < -200f)
                    {
                        Vanishment = true;
                        temp_position = TileBlocks[tiletypelist[arr]][i].transform.position;
                        temp_position.z += 400f;
                    }

                    if (Vanishment)
                    {
                        TileBlocks[tiletypelist[arr]][i].SetActive(false);
                        garbage[tiletypelist[arr]].Enqueue(TileBlocks[tiletypelist[arr]][i]);
                        TileBlocks[tiletypelist[arr]].RemoveAt(i);
                        i--;

                        SpawnTile(random_TileType(), temp_position);
                    }
                }
            }


        }

        private float distance_xz(Vector3 x, Vector3 y)
        {
            //float dist = x.x * x.x + x.y * x.y + x.z * x.z - y.x * y.x - y.y * y.y - y.z * y.z;

            float dist = x.x * x.x + x.z * x.z - y.x * y.x - y.z * y.z;

            if (dist < 0)
                dist = -dist;

            dist = Mathf.Pow(dist, 0.5f);

            return dist;
        }

        private TileScript.TileType random_TileType()
        {

            float maxprob = 0;
            for (int j = 0; j < Tiles.Length; j++)
                maxprob += Probability[j];
            float prob = Random.Range(0, maxprob);

            for (int j = 0; j < Tiles.Length; j++)
            {
                prob -= Probability[j];
                if (prob < 0f)
                {
                    return Tiles[j].GetComponent<TileScript>().getTileType();
                }
            }

            return TileScript.TileType.Nonexistent;
        }



    }
}