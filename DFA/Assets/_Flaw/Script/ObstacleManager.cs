using UnityEngine;
using System.Collections;

public class ObstacleManager : MonoBehaviour
{	

	// This script will take care of the city generation. It will generate the building and the cars once and will
	// then move them accordingly to the player position.
	// The city has a particular pattern as there is always 3 rows of 6 building ahead of the player.
	// One of tow row is shifted in the x axis to block the player point of view.
	// Between each row is a line of car who will all go in the same direction depending on there z position.

	public GameObject player;

	public GameObject Building1;
	public GameObject Building2;
	public GameObject Building3;
	public GameObject Building4;
	public GameObject Building5;
	public GameObject Building6;

	public GameObject Car1;

	// all these gameobject are pretty self-explanatory.
	

	private GameObject[] city; // The array where all the building gameobject will be stored.
	private GameObject[] car; // The array where all the cars will be stored.
	private int Building_Number = 18; // Total number of Building. Changing it wont work.
	private int Car_Number = 72; // Total number of cars. Same as above.
	private int shift = 0; // Global int who will help us to know whether we need to shift the position of our building

	bool CheckRandomBuild(int rand, int[] style)
	{
		// This function will return true if rand is already included in the style array.
		// Otherwise it will return false.
		int i = -1;

		while (++i < 6)
		{
			if (style[i] == rand)
				return (true);
		}
		return (false);
	}

	int[] GetRandomBuild()
	{
		// This function will return a integer array of Random Integer between 0 and 5 because we have 6 type of Buildings.
		// This function also make sur that there wont be the same number twice in the array.

		int[] style = new int[6]; // the array.
		int	i = -1; // the counter.
		int rand = 0; // the random number.

		while (++i < 6)
			style[i] = -1;
		// we initialise every variable of the array with the value -1.
		// This is because we couldn't initialise the array with 0 as we need this array to not have the same value twice
		// (except if this value is not between 0 and 5).

		i = -1; // reset the counter.
		while (++i < 6)
		{
			rand = (int)Mathf.Floor(Random.Range(0.0f, 5.99f)); // We pick a value between 0.0 and 5.99.
			// The floor function will then round down those value and we then cast those value in integer

			while (CheckRandomBuild(rand, style) == true) // while the value picked is already in the array.
				rand = (int)Mathf.Floor(Random.Range(0.0f, 5.99f)); // retry with another value.
			style[i] = rand; // we found a value that wasn't already in the array ! We choose it.
		}
		return (style);
	}

	void SpawnBuilding()
	{
		city = new GameObject[Building_Number]; // The array where we will store our Building GameObject.
		int[] style = new int[6]; // The array that will decide the type of building in each row.
		int i = 0; // Global counter of our building.
		int	j = 0; // row counter.
		int	x = 0; // building type.

		Vector3 pos = new Vector3(-125.0f, 0.0f, 150.0f);
		// As this function will only be called once in the begging of the game, we can predict where we want our
		// buildings to be placed.

		while (i < Building_Number)
		{
			j = i - 1;
			x = 0;
			shift++;
			style = GetRandomBuild(); // Get a random set of buildings.
			while (++j < i + 6)
			{
				// Instantiate all the buildings in a row.
				if (style[x] == 0)
					city[j] = Instantiate(Building1, pos, Quaternion.identity) as GameObject;
				else if (style[x] == 1)
					city[j] = Instantiate(Building2, pos, Quaternion.identity) as GameObject;
				else if (style[x] == 2)
					city[j] = Instantiate(Building3, pos, Quaternion.identity) as GameObject;
				else if (style[x] == 3)
					city[j] = Instantiate(Building4, pos, Quaternion.identity) as GameObject;
				else if (style[x] == 4)
					city[j] = Instantiate(Building5, pos, Quaternion.identity) as GameObject;
				else
					city[j] = Instantiate(Building6, pos, Quaternion.identity) as GameObject;
				// Randomly rotate the building
				if (Random.value > 0.5f)
					city[j].transform.eulerAngles = new Vector3(0.0f, 180.0f, 0.0f);
				else
					city[j].transform.eulerAngles = new Vector3(0.0f, 0.0f, 0.0f);
				pos.x += 50.0f;
				x++;
			}
			pos.x = -125.0f;
			pos.x += (shift % 2 == 0 ? 0.0f : -25.0f); // Depending is shift is an odd number, the position in x of the
			// next row will change.
			pos.z += 75.0f;
			i += 6; // go to the next row.
		}
	}

	void SpawnCar()
	{

		// This function is basically the same as SpawnBuilding();
		car = new GameObject[Car_Number];
		int	i = 0;
		int j = 0;
		Vector3 pos = new Vector3(-200.0f, 0.0f, 187.5f);

		while (i < Car_Number)
		{
			j = i - 1;
			pos.y = Random.Range(-100.0f, 0.0f);
			// Between each row of building, instantiate two line of cars.
			while (++j < i + 24)
			{
				// these two line of car will be in the same position expect one will be 100f higher.
				pos.y += Random.Range(-10.0f, 10.0f); // some car will be a little higher than their neighbours.
				if (j == i + 12) 
				{
					pos.x = -200.0f;
					pos.y += 100.0f;
					// next line : reset the x axis and put the y axis higher.
				}
				car[j] = Instantiate(Car1, pos, Quaternion.identity) as GameObject; // instantiate the car.
				pos.x += 30.0f;
			}
			pos.z += 75.0f; // next row.
			pos.x = -200.0f;
			i += 24;
		}
	}

	void ManageBuilding()
	{
		int i = 0; //Global Counter 
		int	j = 0; // row counter
		int[] style = new int[6]; // array to make our row "random" (building won't have the same place as before).
		Vector3 newCoord = player.transform.position; //position of first building created.

		//The coordinate will be modified.
		//The x coordinate will be put left of the player (because we're placing our buildings starting by the left)
		//The z coordinate is rounded down to make it more "tiled" and put 175f ahead.
		//The y coordinate is rouned down for the same reason as above.
		newCoord.x = (int)newCoord.x - (((int)newCoord.x % 50) + 125.0f);
		newCoord.z = Mathf.Floor(newCoord.z) + 175.0f;
		newCoord.y = Mathf.Floor(newCoord.y);

		//We will go through every row to see if our player isn't ahead of them.
		//If it is, we need to update the position of the row.
		while (i < Building_Number)
		{
			if (city[i].transform.position.z < player.transform.position.z - 50.0f)
			{
				j = -1;
				newCoord.x += (shift % 2 == 0 ? 0.0f : -25.0f); 
				// Depending is shift is an odd number, the position in x of the row will change.
				shift++;
				style = GetRandomBuild();
				// Get randomly generated building style. (it mean that the building with the style 2 will for example be
				// at the position 5 instead of 3.
				while (++j < 6)
				{
					city[i + style[j]].transform.position = newCoord;
					// Then again random rotation
					if (Random.value > 0.5f)
						city[i + style[j]].transform.eulerAngles = new Vector3(0.0f, 180.0f, 0.0f);
					else
						city[i + style[j]].transform.eulerAngles = new Vector3(0.0f, 0.0f, 0.0f);
					newCoord.x += 50.0f;
				}
				return ;
			}
			i += 6;
		}
	}

	void ManageCar()
	{
		//Same pattern as ManageBuilding.
		//It has some differences considering we manage two lines of car in one row but it's still really similar.

		int	i = 0;
		int	j = 0;
		Vector3 newCoord = player.transform.position;
		Vector3 pos;

		newCoord.x = (int)newCoord.x - (((int)newCoord.x % 50) + 200.0f);
		newCoord.z = Mathf.Floor(newCoord.z) + 175.0f;
		newCoord.y = Mathf.Floor(newCoord.y);
		newCoord.y += Random.Range (-100.0f, 0.0f);
		while (i < Car_Number)
		{
			j = i - 1;
			while (++j < i + 24)
			{
				pos = car[j].transform.position;
				if ((int)car[j].transform.position.z % 2 == 0)
					pos.x += 10.0f * Time.deltaTime;
				else
					pos.x -= 10.0f * Time.deltaTime;
				car[j].transform.position = pos;
			}
			if (car[i].transform.position.z < player.transform.position.z - 50.0f)
			{
				j = i - 1;
				while (++j < i + 24)
				{
					newCoord.y += Random.Range(-10.0f, 10.0f);
					if (j == i + 12)
					{
						newCoord.y += 100.0f;
						newCoord.x -= 360.0f; 
					}
					car[j].transform.position = newCoord;
					newCoord.x += 30.0f;
				}
			}
			i += 24;
		}
		
	}

	void Awake()
	{
		Time.timeScale = 1.0f; // make the speed of the game normal.
		RenderSettings.fogEndDistance = 0.0f; // Make the fog really thick (to make a cool transition).

		//Spawn the Building and the cars.
		SpawnBuilding();
		SpawnCar();
	}
	
	void Update()
	{
		float fog = RenderSettings.fogEndDistance;
		// At the beginning of the game, the fog will be really thick but will become normal in a second. Here's how.
		if (fog < 150.0f) // the fog is still too thick
		{
			fog += 150.0f * Time.deltaTime; //add 150f in a second.
			RenderSettings.fogEndDistance = fog; // apply fog;
		}
		// Basic performance saver : One frame out of two, we will update the building position, the other frame we
		// will update the car positions.
		if ((int)Time.frameCount % 2 == 0)
			ManageBuilding();
		else
			ManageCar();
	}
}