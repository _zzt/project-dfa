﻿using UnityEngine;
using System.Collections;

public class Collider_QuadScript : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {


    }

    // Update is called once per frame
    /*
    void Update () {
	
    }*/


    void OnTriggerEnter(Collider other)
    {    //  쿼드 충돌 전용 스크립트.

        Vector3 force = new Vector3();
        float intensity = 500000;

        object[] tempStorage = new object[2];   // 이거 쓸모없어짐.

        if (other.tag == "Player")
        {     // 쿼드에 충돌한게 플레이어이면

            PlayerScript Player_script = other.GetComponent<PlayerScript>();

            if (Player_script.isAttack())
            {
                this.GetComponentInParent<TileScript>().DestroyTile(other.gameObject.transform.position - new Vector3(0f, 100f));
                //Debug.Log("바닥 깨져요");
            }

            TileScript.TileType Tile = this.GetComponentInParent<TileScript>().getTileType();


            if (Tile == TileScript.TileType.Ordinary)
            {

            }
            else if (Tile == TileScript.TileType.Nonexistent)
            {

            }
        }

        if (other.tag == "Cube_Player")
        {
            other.GetComponentInParent<PlayerScript>().SetGround();
        }
    }

    void OnTriggerStay(Collider other) // 죽음을 택하겠다!
    {
        if (other.tag == "Cube_Player")
        {
            PlayerScript Player_script = other.GetComponentInParent<PlayerScript>();

            if (this.GetComponentInParent<TileScript>().isdestroyed())
            {

                if (Player_script.getFloatingState() == FloatingState.Grounding && Player_script.getPlayerState() == PlayerState.Standing)
                {

                    Debug.Log("넌 이미 죽어있다");
                    Player_script.SetDisable();
                    Player_script.SetOMG();
                }
            }
            else if (!this.GetComponentInParent<TileScript>().isused())
            {
                TileScript.TileType Tile = this.GetComponentInParent<TileScript>().getTileType();

                if (Tile == TileScript.TileType.Spring)
                    Player_script.Spring();
            }
        }
    }

    
}
