﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace HAJE.DFA
{
    public class MobManager : MonoBehaviour
    {
        public UnityEngine.Object NormalMob;
        public UnityEngine.Object PullMob;

        private List<GameObject> aliveList = new List<GameObject>();
        private Queue<GameObject> garbage = new Queue<GameObject>();

        private void Update()
        {
            for (int i = 0; i < aliveList.Count; i++)
                if (aliveList[i].transform.position.y < -50f)
                {
                    aliveList[i].SetActive(false);
                    garbage.Enqueue(aliveList[i]);
                    aliveList.RemoveAt(i);
                    i--;
                }
        }
        public void SpawnMob(MobScript.MobType mobType, Vector3 position)
        {
            GameObject mob;
            if (garbage.Count != 0)
            {
                mob = garbage.Dequeue();
                mob.SetActive(true);
                mob.GetComponent<MobScript>().Reset(mobType);
            }
            else
            {
                mob = Instantiate(NormalMob, position, Quaternion.identity) as GameObject;
                mob.GetComponent<MobScript>().Reset(mobType);
            }
            aliveList.Add(mob);

            mob.transform.position = position;
        }
    }
}