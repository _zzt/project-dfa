﻿using UnityEngine;
using System.Collections;

public enum PlayerState
{
    Standing,
    Floating,
    Jump_Attacking,
    Disabled
}

public enum FloatingState
{
    Grounding,
    Floating,
    OMG
}

public class PlayerScript : MonoBehaviour
{

    public FighterAnimationDemo fighterAnimationDemo;



    private PlayerState PlayerState = PlayerState.Standing; // 프라이베트로 수정.
    private FloatingState FloatingState = FloatingState.Grounding;

    const float closeRadius = 50f;
    const float farRadius = 150f;

    const float Time_jump = 1.40f;      // 점프 '만' 시간
    const float Time_jump_Attack = 1.10f;       // 점프-어택 시간.

    bool jump_cancle = false; // 점프 중 다이브 일때.

    int Tag_Number = 0;         // 사기 스킬 태그넘버. 코루틴 전용.

    IEnumerator wait_stand(float time = 0.75f)    // 일정 시간이 지나고 플레이어가 Idel 상태에 가는 시간을 짐작.
    {
        int Tag_this = Tag_Number;

        yield return new WaitForSeconds(time);
        if (Tag_this != Tag_Number)
        {
            Debug.Log("Cancle Standing");
        }
        else
        {
            PlayerState = PlayerState.Standing;
            Debug.Log("To Standing");
        }


    }

    IEnumerator wait_OMG(float time = 0.3f)
    {
        int Tag_this = Tag_Number;

        yield return new WaitForSeconds(time);

        if (Tag_this != Tag_Number)
        {
            Debug.Log("Cancle Standing");
        }
        else
        {
            Debug.Log("To Floating");

            PlayerState = PlayerState.Floating;
        }

        yield return new WaitForSeconds(1.5f);

        if (Tag_this != Tag_Number)
        {
            Debug.Log("Cancle Grounding");
        }
        else
        {
            Debug.Log("넌 아마 죽어있다.");
            SetDisable();
            SetOMG();
        }
    }

    private void CoroutineMaestro(IEnumerator Coroutine){
        Tag_Number++;

        StartCoroutine(Coroutine);
    }

    void Start()
    {
        PlayerState = PlayerState.Floating;
        FloatingState = FloatingState.Floating;
    }

    // Update is called once per frame
    void Update()
    {
        //
        // TODO : 현재 모션이 어떻게 되어 있는지를 보고 착지 상태면 PlayerState를 Standing으로 바꿉시다.
        //

        if (Input.GetMouseButtonDown(0) && (PlayerState == PlayerState.Standing || PlayerState == PlayerState.Floating))
        {       // Standing , Floating.

            //Debug.Log("aa");
            Vector2 pos = Input.mousePosition;    // 터치한 위치
            Vector3 theTouch = new Vector3(pos.x, pos.y, 0.0f);    // 변환 안하고 바로 Vector3로 받아도 되겠지.

            Ray ray = Camera.main.ScreenPointToRay(theTouch);    // 터치한 좌표 레이로 바꾸엉
            RaycastHit hit;    // 정보 저장할 구조체 만들고

            if (Physics.Raycast(ray, out hit, Mathf.Infinity))    // 레이저를 끝까지 쏴블자. 충돌 한넘이 있으면 return true다.
            {
                float dist = Vector2.Distance(hit.point, transform.position);

                if (FloatingState == FloatingState.Grounding)
                {    //  바닥에 있을 경우만
                    if (dist < closeRadius)     // 너무 가까웡!
                    {
                        fighterAnimationDemo.Jump_just(hit, true);
                        PlayerState = PlayerState.Floating;
                        CoroutineMaestro(wait_stand(Time_jump));
                    }
                    else if (dist <= farRadius)  // 적당해
                    {
                        fighterAnimationDemo.Jump_Attack(hit);
                        PlayerState = PlayerState.Jump_Attacking;
                        CoroutineMaestro(wait_stand(Time_jump));
                    }
                    FloatingState = FloatingState.Floating;

                }
                else if (PlayerState != PlayerState.Jump_Attacking)
                {
                    /* if(dist < farRadius){       // 너무 멀엉!
                         fighterAnimationDemo.Dive(hit);
                         PlayerState = PlayerState.Jump_Attacking;
                     }*/
                    fighterAnimationDemo.Dive(hit);
                    PlayerState = PlayerState.Jump_Attacking;
                    jump_cancle = true;         // State 변환 막기! 막기!
                    CoroutineMaestro(wait_stand(Time_jump_Attack));
                }

                /* if (FloatingState == FloatingState.Grounding)
                 {// 떠있으면서
                     PlayerState = PlayerState.Standing;
                 }*/

                //else do nothing
            }


        }
        else if(Input.GetMouseButtonDown(0))
        {
            Debug.Log("Exception " + PlayerState);
            Debug.Log("Exception " + FloatingState);
        }

        //transform.position += new Vector3(0.0f,0.0f, 5.0f * Time.deltaTime);
    }

    public void SetGround()
    {
        if (FloatingState != FloatingState.Grounding)
        {
            FloatingState = FloatingState.Grounding;
            Debug.Log("바닥!");
            fighterAnimationDemo.SetGround();
        }
    }

    public void Spring()
    {
        fighterAnimationDemo.Spring();

        FloatingState = FloatingState.OMG;
        PlayerState = PlayerState.Disabled;

        jump_cancle = true;

        CoroutineMaestro(wait_OMG());

    }

    public void SetFloating()
    {
        PlayerState = PlayerState.Floating;
        FloatingState = FloatingState.Floating;
    }

    public void SetOMG()
    {
        FloatingState = FloatingState.OMG;
    }

    public void SetDisable()
    {
        PlayerState = PlayerState.Disabled;
        fighterAnimationDemo.Die();

    }

    public bool isAttack()
    {
        if (PlayerState == PlayerState.Jump_Attacking)
            return true;

        return false;
    }

    public PlayerState getPlayerState()
    {
        return PlayerState;
    }
    public FloatingState getFloatingState()
    {
        return FloatingState;
    }

    //IEnumerator 
}
