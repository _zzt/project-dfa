﻿using UnityEngine;
using System.Collections;

public class Collide_CubeScript : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    // 일단은 쓰지 않는 코드.


    void OnTriggerStay(Collider other) // 죽음을 택하겠다!
    {
        if (other.tag == "Player")
        {
            PlayerScript Player_script = other.GetComponentInParent<PlayerScript>();

            if (this.GetComponentInParent<TileScript>().isdestroyed())
            {

                if (Player_script.getFloatingState() == FloatingState.Grounding && Player_script.getPlayerState() == PlayerState.Standing)
                {

                    Debug.Log("넌 이미 죽어있다");
                    other.GetComponent<PlayerScript>().SetDisable();
                    other.GetComponent<PlayerScript>().SetOMG();
                }
            }
            else if (!this.GetComponentInParent<TileScript>().isused())
            {
                TileScript.TileType Tile = this.GetComponentInParent<TileScript>().getTileType();

                if (Tile == TileScript.TileType.Spring)
                    Player_script.Spring();
            }
        }
    }
}
