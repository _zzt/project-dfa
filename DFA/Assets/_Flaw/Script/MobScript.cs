﻿using UnityEngine;
using System.Collections;

namespace HAJE.DFA
{
    public class MobScript : MonoBehaviour
    {
        public enum DeathReason
        {
            Alive,
            Falldown,
            Shockwave,
        }
        public enum MobType
        {
            Normal,
            Pull,
        }

        public float Speed;
        public float RefreshTime;
        public float RefreshTimeError;
        public float PullingForce;
        public MobType mobType;

        private bool isAlive;
        private bool isPulling;
        private bool isPulled;

        private Quaternion targetQuaternion;
        private DeathReason deathReason;

        private Rigidbody rb;
        private float timer;
        private GameObject player;

        public void Reset(MobType mobType)
        {
            this.mobType = mobType;
            this.deathReason = DeathReason.Alive;
            this.isAlive = true;
            this.isPulling = false;
            this.isPulled = false;
            this.transform.rotation = Random.rotation;
        }
        private void Start()
        {
            this.rb = this.GetComponent<Rigidbody>();
            this.timer = RefreshTime + RefreshTimeError * Random.value;
            this.player = GameObject.FindGameObjectWithTag("Player");
            this.isAlive = true;
            this.isPulling = false;
            this.isPulled = false;
            this.deathReason = DeathReason.Alive;
            this.transform.rotation = Random.rotation;
        }

        private void RefreshDirection()
        {
            this.targetQuaternion = this.transform.rotation * Quaternion.Euler(30 * Random.value, 30 * Random.value, 30 * Random.value);
        }
        private void Update()
        {
            this.timer -= Time.deltaTime;

            if (this.isAlive == true)
            {
                if (this.timer < 0f)
                {
                    RefreshDirection();
                    this.timer = RefreshTime + RefreshTimeError * Random.value;
                }
                this.transform.rotation = Quaternion.Slerp(this.transform.rotation, this.targetQuaternion, 0.05f);

                if (this.mobType == MobType.Pull)
                {
                    if (this.isPulling == false && this.isPulled == false)
                        if ((this.transform.position - player.transform.position).magnitude < 20)
                        {
                            isPulled = true;
                            isPulling = true;
                        }
                }
            }
            else
            {
                if (this.timer < 0f)
                    Destroy(this.gameObject);
            }
        }
        private void FixedUpdate()
        {
            if (this.isAlive == true)
            {
                if (this.isPulling == false)
                {
                    rb.AddTorque(this.transform.rotation * Vector3.forward * Speed);
                }
                else
                {
                    Vector3 v3 = player.transform.position - this.transform.position;
                    v3.Normalize();
                    player.GetComponent<Rigidbody>().AddForce(-v3 * PullingForce);
                }
            }
        }
        private void OnCollisionEnter(Collision collision)
        {
            if (isPulling == true)
                if (collision.gameObject.tag == "Player")
                    isPulling = false;
        }

        public void Kill(DeathReason dr)
        {
            this.isAlive = false;
            this.deathReason = dr;
            this.timer = 20f;

            Rigidbody prb = player.GetComponent<Rigidbody>();
            Vector3 v3 = 5 * Vector3.up + (rb.position - prb.position);
            rb.AddTorque(25 * new Vector3(Random.value, Random.value, Random.value));
            rb.AddForce(v3 * 100);
        }
    }
}