﻿using UnityEngine;
using System.Collections;

public class MoveManager : MonoBehaviour
{

    [SerializeField]
    protected float waitTime = 0;
    [SerializeField]
    protected float time = 1f;
    [SerializeField]
    protected float dive_time = 0.7f;

    [SerializeField]
    GameObject msgTarget;
    [SerializeField]
    string msgMethodName = null;
    [SerializeField]
    protected AnimationCurve curve_xz = AnimationCurve.Linear(0, 0, 1, 1);

    [SerializeField]
    protected AnimationCurve curve_y = AnimationCurve.Linear(0, 0, 1, 1);

    [SerializeField]
    protected AnimationCurve curve_dive_xz = AnimationCurve.Linear(0, 0, 1, 1);

    [SerializeField]
    protected AnimationCurve curve_dive_y = AnimationCurve.Linear(0, 0, 1, 1);

    [SerializeField]
    protected AnimationCurve curve_jumpdive_y = AnimationCurve.Linear(0, 0, 1, 1);

    [SerializeField]
    protected AnimationCurve curve_spring_y = AnimationCurve.Linear(0, 0, 1, 1);

    [SerializeField]
    protected AnimationCurve curve_rot = AnimationCurve.Linear(0, 0, 1, 1);

    [SerializeField]
    protected AnimationCurve curve_scale = AnimationCurve.Linear(0, 0, 1, 1);


    [SerializeField]
    protected AnimationCurve curve_start = AnimationCurve.Linear(0, 0, 5, 1);


    [SerializeField]
    bool OnPlay = false;
    [SerializeField]
    bool OnDestroy = false;

    Vector3 start;
    Quaternion start_rot;

    [SerializeField]
    public Vector3 dest;

    [SerializeField]
    Quaternion dest_rot;

    [SerializeField]
    bool isLocal = false;

    Vector3 parent;

    public bool stopFlag = false;

    int TagNumber = 0;  // 숫자 순서 개꿀!

    void init()
    {
        isInit = true;

        if (curve_xz.length < 2)
        { curve_xz = AnimationCurve.Linear(0, 0, 1, 1); }
        if (curve_y.length < 2)
        { curve_y = AnimationCurve.Linear(0, 0, 1, 1); }
        if (curve_dive_y.length < 2)
        { curve_dive_y = AnimationCurve.Linear(0, 0, dive_time, 1); }
        if (curve_dive_y.length < 2)
        { curve_dive_y = AnimationCurve.Linear(0, 0, dive_time, 1); }
        if (curve_jumpdive_y.length < 2)
        { curve_jumpdive_y = AnimationCurve.Linear(0, 0, 1, 1); }
        if (curve_spring_y.length < 2)
        { curve_spring_y = AnimationCurve.Linear(0, 0, dive_time, 1); }
        if (curve_rot.length < 2)
        { curve_rot = AnimationCurve.Linear(0, 0, 1, 1); }
        if (curve_scale.length < 2)
        { curve_scale = AnimationCurve.Linear(0, 0, 1, 1); }
        if (curve_start.length < 2)
        { curve_start = AnimationCurve.Linear(0, 0, 5, 1); }


        start = transform.position;
        start_rot = transform.rotation;

        if (isLocal == true)
            parent = transform.parent.position;
        else
            parent = Vector3.zero;
    }
    protected virtual void OnEnable()
    {
        if (OnPlay)
        {
            TurnOn();
        }
    }

    protected bool isInit = false;

    private bool bang = false;

    IEnumerator TweenSequence(bool Dive = false, bool Jump = true, bool Spring = false)
    {
        init();

        int Tag_this = TagNumber;

        if (waitTime > 0f)
            yield return new WaitForSeconds(waitTime);

        float curveOutput_xz;
        float curveOutput_y;
        float curveOutput_rot;
        float curveOutput_scale;

        float endTime = 1f;
        float t = 0;

        if (Dive && !Jump)
            endTime = dive_time;

        do
        {
            while (t < endTime && Tag_this == TagNumber)
            {
                t += Time.deltaTime / time;
                curveOutput_xz = curve_xz.Evaluate(t);

                if (Spring)
                {
                    curveOutput_y = curve_spring_y.Evaluate(t);
                }
                else if (!Dive)
                {   // 다이브아니면? 걍점프.
                    curveOutput_y = curve_y.Evaluate(t);
                }
                else if (!Jump)
                {        // 다이브 맞는데 점프아니면? 그냥 다이브.
                    curveOutput_y = curve_dive_y.Evaluate(t);
                    curveOutput_xz = curve_dive_xz.Evaluate(t);
                }
                else        // 둘다 아니면? 점프 찍기지 뭐.
                    curveOutput_y = curve_jumpdive_y.Evaluate(t);
                //positiontween


                transform.position = new Vector3(((1f - curveOutput_xz) * start.x + curveOutput_xz * dest.x) + parent.x, ((1f - curveOutput_y) * start.y + curveOutput_y * dest.y) + parent.y, ((1f - curveOutput_xz) * start.z + curveOutput_xz * dest.z) + parent.z);

                //transform.position = ((1f - curveOutput_xz) * start + curveOutput_xz * dest) + parent;

                curveOutput_rot = curve_rot.Evaluate(t);

                transform.rotation = new Quaternion(
               start_rot.x * (1f - curveOutput_rot) + dest_rot.x * curveOutput_rot,
               start_rot.y * (1f - curveOutput_rot) + dest_rot.y * curveOutput_rot,
               start_rot.z * (1f - curveOutput_rot) + dest_rot.z * curveOutput_rot,
               start_rot.w * (1f - curveOutput_rot) + dest_rot.w * curveOutput_rot
               );


                yield return null;
            }

            if (curve_xz.postWrapMode == WrapMode.Loop || curve_xz.postWrapMode == WrapMode.PingPong)
            {
                endTime += 1f;
            }
            else
            {
                break;
            }
            yield return null;
        } while (!stopFlag);

        if (msgMethodName == null)
            yield break;

        string msg = msgMethodName.Trim(' ');
        if (msg.Length != 0)
        {
            if (msgTarget == null)
                SendMessage(msg);
            else
                msgTarget.SendMessage(msg);
        }


        if (OnDestroy)
            Destroy(this);
    }


    public Coroutine TurnOn(bool Dive = false, bool Jump = true, bool Spring = false)
    {

        TagNumber++;

        return StartCoroutine(TweenSequence(Dive, Jump, Spring));
    }
    public Quaternion Dest { get { return dest_rot; } set { dest_rot = value; } }


    public Coroutine Start()
    {
        return StartCoroutine(StartSequence());
    }

    IEnumerator StartSequence()
    {
        init();

        int Tag_this = TagNumber;

        if (waitTime > 0f)
            yield return new WaitForSeconds(waitTime);

        float curveOutput_xz;
        float curveOutput_y;
        float curveOutput_rot;

        float endTime = 5.0f;
        float t = 0;

        do
        {
            while (t < endTime && Tag_this == TagNumber)
            {
                t += Time.deltaTime / time;
                curveOutput_xz = curve_xz.Evaluate(t);

                curveOutput_y = curve_start.Evaluate(t);
                //positiontween


                transform.position = new Vector3(((1f - curveOutput_xz) * start.x + curveOutput_xz * dest.x) + parent.x, ((1f - curveOutput_y) * start.y + curveOutput_y * dest.y) + parent.y, ((1f - curveOutput_xz) * start.z + curveOutput_xz * dest.z) + parent.z);

                //transform.position = ((1f - curveOutput_xz) * start + curveOutput_xz * dest) + parent;

                curveOutput_rot = curve_rot.Evaluate(t);

                transform.rotation = new Quaternion(
               start_rot.x * (1f - curveOutput_rot) + dest_rot.x * curveOutput_rot,
               start_rot.y * (1f - curveOutput_rot) + dest_rot.y * curveOutput_rot,
               start_rot.z * (1f - curveOutput_rot) + dest_rot.z * curveOutput_rot,
               start_rot.w * (1f - curveOutput_rot) + dest_rot.w * curveOutput_rot
               );


                yield return null;
            }

            if (curve_xz.postWrapMode == WrapMode.Loop || curve_xz.postWrapMode == WrapMode.PingPong)
            {
                endTime += 1f;
            }
            else
            {
                break;
            }
            yield return null;
        } while (!stopFlag);

        if (msgMethodName == null)
            yield break;

        string msg = msgMethodName.Trim(' ');
        if (msg.Length != 0)
        {
            if (msgTarget == null)
                SendMessage(msg);
            else
                msgTarget.SendMessage(msg);
        }


        if (OnDestroy)
            Destroy(this);
    }
}
